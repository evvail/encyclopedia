package edu.washington.gs.maccoss.encyclopedia.algorithms;

import edu.washington.gs.maccoss.encyclopedia.datastructures.LibraryEntry;

public class RescoredPeptideScoringResult extends PeptideScoringResult {
	public RescoredPeptideScoringResult(LibraryEntry entry) {
		super(entry);
	}
}
