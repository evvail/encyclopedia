package edu.washington.gs.maccoss.encyclopedia.algorithms.library;

public interface LibraryBackgroundInterface {

	float getFraction(double mass);

}