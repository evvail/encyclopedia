package edu.washington.gs.maccoss.encyclopedia.utils.graphing;

public enum GraphType {
	area, line, dashedline, boldline, squaredline, bigpoint, point, spectrum, tinypoint, text;
}
