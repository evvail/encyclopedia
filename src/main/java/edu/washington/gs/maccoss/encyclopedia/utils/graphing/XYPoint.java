package edu.washington.gs.maccoss.encyclopedia.utils.graphing;


//@Immutable
public class XYPoint implements PointInterface {
	public final double x;
	public final double y;

	public XYPoint(double x, double y) {
		this.x=x;
		this.y=y;
	}
	
	@Override
	public String toString() {
		return x+","+y;
	}

	@Override
	public double getX() {
		return x;
	}

	@Override
	public double getY() {
		return y;
	}

	/**
	 * compares on X first then on Y
	 */
	@Override
	public int compareTo(PointInterface o) {
		if (o==null) return 1;
		if (x>o.getX()) return 1;
		if (x<o.getX()) return -1;
		if (y>o.getY()) return 1;
		if (y<o.getY()) return -1;
		return 0;
	}
	
	@Override
	public int hashCode() {
		return Double.hashCode(x)+Double.hashCode(y);
	}
	
	@Override
	public boolean equals(Object obj) {
		return compareTo((PointInterface)obj)==0;
	}
}
